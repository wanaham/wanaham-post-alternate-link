<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/admin
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Post_Alternate_Link_Admin {

    // Locale list taken from:
    // http://stackoverflow.com/questions/3191664/
    // list-of-all-locales-and-their-short-codes
    private $locales = array(
        'af-ZA',
        'am-ET',
        'ar-AE',
        'ar-BH',
        'ar-DZ',
        'ar-EG',
        'ar-IQ',
        'ar-JO',
        'ar-KW',
        'ar-LB',
        'ar-LY',
        'ar-MA',
        'arn-CL',
        'ar-OM',
        'ar-QA',
        'ar-SA',
        'ar-SY',
        'ar-TN',
        'ar-YE',
        'as-IN',
        'az-Cyrl-AZ',
        'az-Latn-AZ',
        'ba-RU',
        'be-BY',
        'bg-BG',
        'bn-BD',
        'bn-IN',
        'bo-CN',
        'br-FR',
        'bs-Cyrl-BA',
        'bs-Latn-BA',
        'ca-ES',
        'co-FR',
        'cs-CZ',
        'cy-GB',
        'da-DK',
        'de-AT',
        'de-CH',
        'de-DE',
        'de-LI',
        'de-LU',
        'dsb-DE',
        'dv-MV',
        'el-GR',
        'en-029',
        'en-AU',
        'en-BZ',
        'en-CA',
        'en-GB',
        'en-IE',
        'en-IN',
        'en-JM',
        'en-MY',
        'en-NZ',
        'en-PH',
        'en-SG',
        'en-TT',
        'en-US',
        'en-ZA',
        'en-ZW',
        'es-AR',
        'es-BO',
        'es-CL',
        'es-CO',
        'es-CR',
        'es-DO',
        'es-EC',
        'es-ES',
        'es-GT',
        'es-HN',
        'es-MX',
        'es-NI',
        'es-PA',
        'es-PE',
        'es-PR',
        'es-PY',
        'es-SV',
        'es-US',
        'es-UY',
        'es-VE',
        'et-EE',
        'eu-ES',
        'fa-IR',
        'fi-FI',
        'fil-PH',
        'fo-FO',
        'fr-BE',
        'fr-CA',
        'fr-CH',
        'fr-FR',
        'fr-LU',
        'fr-MC',
        'fy-NL',
        'ga-IE',
        'gd-GB',
        'gl-ES',
        'gsw-FR',
        'gu-IN',
        'ha-Latn-NG',
        'he-IL',
        'hi-IN',
        'hr-BA',
        'hr-HR',
        'hsb-DE',
        'hu-HU',
        'hy-AM',
        'id-ID',
        'ig-NG',
        'ii-CN',
        'is-IS',
        'it-CH',
        'it-IT',
        'iu-Cans-CA',
        'iu-Latn-CA',
        'ja-JP',
        'ka-GE',
        'kk-KZ',
        'kl-GL',
        'km-KH',
        'kn-IN',
        'kok-IN',
        'ko-KR',
        'ky-KG',
        'lb-LU',
        'lo-LA',
        'lt-LT',
        'lv-LV',
        'mi-NZ',
        'mk-MK',
        'ml-IN',
        'mn-MN',
        'mn-Mong-CN',
        'moh-CA',
        'mr-IN',
        'ms-BN',
        'ms-MY',
        'mt-MT',
        'nb-NO',
        'ne-NP',
        'nl-BE',
        'nl-NL',
        'nn-NO',
        'nso-ZA',
        'oc-FR',
        'or-IN',
        'pa-IN',
        'pl-PL',
        'prs-AF',
        'ps-AF',
        'pt-BR',
        'pt-PT',
        'qut-GT',
        'quz-BO',
        'quz-EC',
        'quz-PE',
        'rm-CH',
        'ro-RO',
        'ru-RU',
        'rw-RW',
        'sah-RU',
        'sa-IN',
        'se-FI',
        'se-NO',
        'se-SE',
        'si-LK',
        'sk-SK',
        'sl-SI',
        'sma-NO',
        'sma-SE',
        'smj-NO',
        'smj-SE',
        'smn-FI',
        'sms-FI',
        'sq-AL',
        'sr-Cyrl-BA',
        'sr-Cyrl-CS',
        'sr-Cyrl-ME',
        'sr-Cyrl-RS',
        'sr-Latn-BA',
        'sr-Latn-CS',
        'sr-Latn-ME',
        'sr-Latn-RS',
        'sv-FI',
        'sv-SE',
        'sw-KE',
        'syr-SY',
        'ta-IN',
        'te-IN',
        'tg-Cyrl-TJ',
        'th-TH',
        'tk-TM',
        'tn-ZA',
        'tr-TR',
        'tt-RU',
        'tzm-Latn-DZ',
        'ug-CN',
        'uk-UA',
        'ur-PK',
        'uz-Cyrl-UZ',
        'uz-Latn-UZ',
        'vi-VN',
        'wo-SN',
        'xh-ZA',
        'yo-NG',
        'zh-CN',
        'zh-HK',
        'zh-MO',
        'zh-SG',
        'zh-TW',
        'zu-ZA',);

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */

    private $_meta_value_key = '';
    private $_inner_custom_box = '';
    private $_inner_custom_box_nonce = '';

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

        $_meta_value_key = str_replace('-','_',$plugin_name).'_meta_value_key';
        $_inner_custom_box = str_replace('-','_',$plugin_name).'inner_custom_box';
        $_inner_custom_box_nonce = str_replace('-','_',$plugin_name).'inner_custom_box_nonce';

        $this->_meta_value_key = $_meta_value_key;
        $this->_inner_custom_box = $_inner_custom_box;
        $this->_inner_custom_box_nonce = $_inner_custom_box_nonce;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Post_Alternate_Link_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Post_Alternate_Link_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wanaham-post-alternate-link-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Post_Alternate_Link_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Post_Alternate_Link_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wanaham-post-alternate-link-admin.js', array( 'jquery' ), $this->version, false );

	}

    public function setup_meta_boxes_setup(){

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
        add_action( 'save_post', array( $this, 'save_meta_boxes' ) );
    }

    public function add_meta_boxes($post_type){

        $post_types = array('post', 'page');     //limit meta box to certain post types
        if ( in_array( $post_type, $post_types )) {
            add_meta_box(
                'wanaham-post-alternate-link',      // Unique ID
                __( 'Liens alternatifs', $this->plugin_name ),    // Title
                array($this,'render_meta_box_content'),   // Callback function
                $post_type,         // Admin page (or post type)
                'normal',         // Context
                'high'         // Priority
            );
        }
    }
    public function get_post_meta($post_id){
        return get_post_meta( $post_id, $this->_meta_value_key, true );
    }

    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post ) {

               // Add an nonce field so we can check for it later.
        $nonce = wp_nonce_field($this->_inner_custom_box, $this->_inner_custom_box_nonce, true , false);

        // Use get_post_meta to retrieve an existing value from the database.
        $values = $this->get_post_meta($post->ID);

        // Display the form, using the current value.

        $base = '<div class="wanaham_alternate_link_box%s">
        <a href="javascript:void(0);" title="'.__("Delete this link ?",$this->plugin_name).'" class="button"><span class="dashicons dashicons-no-alt"></span></a>
        <label>'.__( 'The locale code', $this->plugin_name ).'</label><select class="wanaham_alternate_hreflang_id" type="text" %s>%s</select><p class="%s">'.__("Not a valid locale",$this->plugin_name).'</p>
        <label>'.__( 'The URI associate', $this->plugin_name ).'</label><input class="wanaham_alternate_link_id" type="text" %s%s placeholder="'.__("The URI associate to the locale",$this->plugin_name).'" size="30" /><p class="%s">'.__("Not a valid URI",$this->plugin_name).'</p>
        </div>';

        $boxes = '';

        if(!empty($values)){
            foreach($values as $index=>$row){
                $link = $row['link'];
                $hreflang = $row['hreflang'];
                $errorClassLink = $row['errors']['link'] === true ?'visible':'';
                $errorClassHrefLang = $row['errors']['hreflang'] === true ?'visible':'';
                $boxes .= sprintf($base,'',
                    'name="wanaham_alternate_row['.$index.'][hreflang]" ',
                    $this->getLocalesToHtmlOptions($hreflang),
                    $errorClassHrefLang,
                    'name="wanaham_alternate_row['.$index.'][link]" ',
                    'value="'.$link.'" ',
                    $errorClassLink);
            }
        }

        echo $nonce,
            '<div class="wanaham_alternate_add_btn_container"><input type="button" value="'.__("Add a link",$this->plugin_name).'" id="wanaham_alternate_link_hreflang" class="button"/></div>',
        sprintf($base,' hidden','',$this->getLocalesToHtmlOptions(),'','','','',''),
        $boxes;


    }

    private function getLocalesToHtmlOptions($current = false){
        $options = '<option value="">'.__("Choose a locale please", $this->plugin_name).'</option>';
        foreach($this->locales as $locale){
            $selected = $locale === $current ? ' selected="selected" ':'';
            $options .= sprintf('<option value="%s"%s>%s</option>',$locale,$selected,$locale);
        }
        return $options;
    }

    /**
     * Save the meta when the post is saved.
     *
     * @param int $post_id The ID of the post being saved.
     */
    public function save_meta_boxes( $post_id ) {

        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */

        // Check if our nonce is set.
        if ( ! isset( $_POST[$this->_inner_custom_box_nonce] ) )
            return $post_id;

        $nonce = $_POST[$this->_inner_custom_box_nonce];

        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, $this->_inner_custom_box ) )
            return $post_id;

        // If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;

        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) )
                return $post_id;

        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) )
                return $post_id;
        }

        /* OK, its safe for us to save the data now. */

        $values = [];
        if(isset($_POST['wanaham_alternate_row']) && is_array($_POST['wanaham_alternate_row'])){
            foreach($_POST['wanaham_alternate_row'] as $box){
                if(isset($box['link']) && isset($box['hreflang'])){
                    $box['link'] = sanitize_text_field( $box['link'] );
                    $box['hreflang'] = sanitize_text_field( $box['hreflang'] );
                    $box['errors'] = array(
                        'link' => false,
                        'hreflang' => false
                    );
                    if(filter_var($box['link'], FILTER_VALIDATE_URL) === false){
                        $box['errors']['link'] = true;
                    }
                    if(!in_array($box['hreflang'],$this->locales)){
                        $box['errors']['hreflang'] = true;
                    }
                    $values[] = $box;
                }
            }
        }

        // Update the meta field.
        update_post_meta( $post_id,  $this->_meta_value_key, $values );
    }

}
