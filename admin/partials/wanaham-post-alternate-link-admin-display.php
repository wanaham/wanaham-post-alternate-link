<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
