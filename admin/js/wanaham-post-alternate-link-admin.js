(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */
    $(document).ready(function(){
        $('.wanaham_alternate_link_box a').click(function(event ) {
            event.stopPropagation();
            if(confirm($(this).attr('title'))){
                $(this).parent().remove();
            }
        });
        $('#wanaham_alternate_link_hreflang').click(function(){
            var nb = $('#wanaham-post-alternate-link .wanaham_alternate_link_box').length-1;// remove the template
            $( "#wanaham-post-alternate-link  .wanaham_alternate_link_box.hidden" ).clone(true, true).removeClass('hidden').appendTo('#wanaham-post-alternate-link .inside');
            $('#wanaham-post-alternate-link .inside .wanaham_alternate_link_box:last-child input.wanaham_alternate_hreflang_id').attr('name','wanaham_alternate_row['+nb+'][hreflang]');
            $('#wanaham-post-alternate-link .inside .wanaham_alternate_link_box:last-child input.wanaham_alternate_link_id').attr('name','wanaham_alternate_row['+nb+'][link]');
        });
    });
})( jQuery );
