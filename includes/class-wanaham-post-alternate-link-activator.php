<?php

/**
 * Fired during plugin activation
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/includes
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Post_Alternate_Link_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
