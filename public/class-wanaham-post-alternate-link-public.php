<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.wanaham.com
 * @since      1.0.0
 *
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wanaham_Post_Alternate_Link
 * @subpackage Wanaham_Post_Alternate_Link/public
 * @author     Wanaham <wanaham.mail@gmail.com>
 */
class Wanaham_Post_Alternate_Link_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Post_Alternate_Link_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Post_Alternate_Link_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wanaham-post-alternate-link-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wanaham_Post_Alternate_Link_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wanaham_Post_Alternate_Link_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wanaham-post-alternate-link-public.js', array( 'jquery' ), $this->version, false );

	}

    public function get_alternate_links(){

        $plugin_admin = new Wanaham_Post_Alternate_Link_Admin( $this->plugin_name, $this->version );

        $post = get_post();

        $values = $plugin_admin->get_post_meta($post->ID);

        if(is_array($values) && count($values)){
            foreach($values as $value){
                echo sprintf('<link rel="alternate" hreflang="%s" href="%s"/>',strtolower($value['hreflang']),$value['link']);
            }
        }
    }

}
